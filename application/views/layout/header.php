<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CarSale</title>
	<link rel="shortcut icon" href="<?=base_url();?>libre/assets/img/icono.png"/>
	<!------------>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" crossorigin="anonymous">
	<link rel="stylesheet" href="<?=base_url('libre/css/normalize.css');?>">
	<link rel="stylesheet" href="<?=base_url('libre/css/sweetalert2.css');?>">
	<link rel="stylesheet" href="<?=base_url('libre/css/material.min.css');?>">
	<link rel="stylesheet" href="<?=base_url('libre/css/material-design-iconic-font.min.css');?>">
	<link rel="stylesheet" href="<?=base_url('libre/css/jquery.mCustomScrollbar.css');?>">
	<link rel="stylesheet" href="<?=base_url('libre/css/main.css');?>">


	<!--librerias--->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="<?=base_url('libre/js/material.min.js');?>" ></script>
	<script src="<?=base_url('libre/js/sweetalert2.min.js');?>" ></script>
	<script src="<?=base_url('libre/js/jquery.mCustomScrollbar.concat.min.js');?>" ></script>
	<script src="<?=base_url('libre/js/main.js');?>" ></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	
</head>