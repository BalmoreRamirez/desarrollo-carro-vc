  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="<?= base_url();?>libre/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url();?>libre/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url();?>libre/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url();?>libre/css/dataTables.bootstrap4.min.css">
  <script src="<?= base_url();?>libre/js/jquery.dataTables.min.js"></script>
  <script src="<?= base_url();?>libre/js/dataTables.bootstrap4.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">/>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>


  <style type="text/css">
  .sa{
  	width: 1100px;
  	height: 700px;
  	border-color: #eeebeb;
  	position: relative;
  	border: 1px solid rgba(0,0,0,.11);
  	border-collapse: collapse;
  	white-space: nowrap;
  	font-size: 13px;
  	background-color: #fff;
  	border-radius: 20px;
  }
  .img{
  	width:40px;
  	margin:0px;
  }
  .s{
  	width: 1070px;
  }
  .sas{
  	width: 1050px;
  }

</style>
<section class="full-width pageContent">
	<section class="full-width">
		<div class="container">
			<div class="row my-3">
				<div class="col-lg-12">
					<div class="sa">
						<div class="">
							<div class="mdl-cell mdl-cell--12-col mdl-cell--0-col-tablet">
								<div class="mdl-grid">
									<center>
										<table class="mdl-data-table mdl-js-data-table s">
											<tbody>
												<tr>
													<td class="mdl-data-table__cell--non-numeric"><img class="img" src="http://localhost/desarrollo-carro-vc/libre/assets/img/logo.jpg"></td>
													<td><?=$user->nombre?></td>
												</tr>
											</tbody>
										</table>
									</center>
								</div>
								<div class="mdl-cell mdl-cell--12-col mdl-cell--0-col-tablet">
									<div class="col-lg-12 mon">
										<table class="sas">
											<thead>
												<tr>
													<th><h4 ><?php echo date("d-M-Y");?></h4></th>
													<th> <h4 class="text-right">FACTURA #<?php echo $data = ($id_fac->num_factura+1); ?></h4></th>
												</tr>
											</thead>
										</table><hr>
										<table class="sas">
											<thead>
												<tr>
													<th>
														<button type="button" class="btn btn-dark add_shopping_cart text-right" data-toggle="modal" data-target="#exampleModal"><i class="zmdi zmdi-account-o zmd-fw"></i> Agregar Cliente</button></th>
														<th><a href="#" data-toggle="modal" data-target=".bd-example-modal-lg" class="btn btn-dark add_shopping_cart text-right"><i class="zmdi zmdi-search zmd-fw"></i> Buscar Auto</a></th>
														<form id="registo">
															<th>
																<select id="disease" name="cliente" class="form-control">
																	<?php 	foreach ($Cliente as $key) {?>
																		<option value="<?php echo $key['id_cliente'];?>"><?php echo $key['nit'];?></option>
																	<?php } ?>
																</select>
															</th>
															<th>
																<select id="vendedor" name="vendedor" class="form-control">
																	<?php 	foreach ($ven as $key) {?>
																		<option value="<?php echo $key['id_vendedor'];?>"><?php echo $key['codigo'].' '.$key['nombre'].' '.$key['apellido'];?></option>
																	<?php } ?>
																</select>
															</th>
															<th>
																<select id="MetodoPago" name="pago" class="form-control">
																	<?php 	foreach ($MetodoPago as $key) {?>
																		<option value="<?php echo $key['id_metodo'];?>"><?php echo $key['pago'];?></option>
																	<?php } ?>
																</select>
															</th>
															<th>

															</th>
														</form>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									<div class="container">
										<center>
											<table class="mdl-data-table mdl-js-data-table sas" border="0">
												<thead>
													<tr>
														<th style="background-color: #3F51B5;">
															<h5 style="color:#fbfcfc;text-align:left;">Cantidad</h5>
														</th>
														<th style="background-color: #3F51B5;">
															<h5 style="color:#fbfcfc;text-align:left;">Detalles</h5>
														</th>
														<th style="background-color: #3F51B5;">
															<h5 style="color:#fbfcfc; text-align:left;">color</h5>
														</th>
														<th style="background-color: #3F51B5;">
															<h5 style="color:#fbfcfc; text-align:left;">Precio Unitario</h5>
														</th>
														<th style="background-color: #3F51B5;">
															<h5 style="color:#fbfcfc; text-align: right;">Total</h5>
														</th>
														<th style="background-color: #3F51B5;">
															<h5 style="color:#fbfcfc; text-align: right;">Sucursal</h5>
														</th>
														<th style="background-color: #3F51B5;">
														</th>
													</tr>



												</thead>
												<tbody >
													<?php foreach ($auto as $key) {?>
														<tr>
															<td class="mdl-data-table__cell" style="background-color: ;">
																<h6 style="text-align:left;"><?=$key['suma']?></h6>
															</td>
															<td><h6 style="text-align:left;"><?=$key['marca']?> <?=$key['Modelo']?> <?=$key['año']?></h6></td>
															<td id="unidad"><h6 style="text-align:left;"><?=$key['color']?></h6></td>
															<td id="unidad"><h6 style="text-align:left;">$<?=$key['Monto']?></h6></td>
															<td ><h6 style="text-align:right;">$ <?=$total = $key['suma'] * $key['Monto']; ?></h6></td>
															<td ><h6 style="text-align:right;"><?=$key['nombre'];?></td>
																<td>
																	<a class="btn btn-outline-danger" id="btn-exit" href='<?= base_url();?>ExisProducCtr/delete/<?php echo $key['id_carrito'];?>'>
																		<i class="zmdi zmdi-delete zmd-fw"></i>
																	</a></td>
																</tr>
															<?php } ?>
															<tr>
																<td></td>
																<td></td>
																<td></td>
																<td><h6 style="text-align:left;">IVA :</h6></td>
																<td><h6 style="text-align:right;">$ <?=$iva=($suma_total->suma_total * 0.013)?></h6></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td></td>
																<td></td>
																<td></td>
																<td><h6 style="text-align:left;">Total :</h6></td>
																<td><h6 style="text-align:right;">$ <?=$aPagar = ($suma_total->suma_total+$iva)?></h6></td>
																<td></td>
																<td><button type="button" onclick="guardar_factura()" class="btn btn-success">Comprar</button></td>
															</tr>
														</tbody>
													</table>
													<h3> <?php if($this->session->flashdata('error')){?></h3>
													<div class="alert alert-danger"><?= $this->session->flashdata('error')?></div>
												<?php }?>
											</center>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>      
				</section>
			</section>
			<!-- Modal -->
			<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="mdl-cell mdl-cell--12-col mdl-cell--0-col-tablet">
							<center>
								<table id="example" class="table table-striped table-bordered" style="width:100%">
									<thead>
										<tr>
											<th>id</th>
											<th>Marca </th>
											<th>Modelo</th>
											<th>Año</th>
											<th>Estiilo</th>
											<th>Motor</th>
											<th>Cilindros</th>
											<th>Combustribre</th>
											<th>Color</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($autos as $key) {?>          
											<tr>
												<td><?php echo $key['id_auto'];?></td>
												<td><?php echo $key['marca'];?></td>
												<td><?php echo $key['Modelo'];?></td>
												<td><?php echo $key['año'];?></td>
												<td><?php echo $key['tipo'];?></td>
												<td><?php echo $key['tipo_motor'];?></td>
												<td><?php echo $key['cilindros'];?></td>
												<td><?php echo $key['color'];?></td>
												<td><?php echo $key['combustible'];?></td>
												<td>
													<center>
														<a class="btn btn-success add_shopping_cart" id="btn-exit" href='<?= base_url();?>ExisProducCtr/ver_aut/<?php echo $key['id_sau'];?>'>
															<i class="zmdi zmdi-shopping-cart-add zmd-fw"></i>
														</a>
													</center>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>

							</center>
						</div>
					</div>
				</div>
			</div>


			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Clientes</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">

							<form id="form_cliente">
								Nombre:<input type="text" name="nombre" id="nombre" placeholder="Digite su nombre" class="form-control">
								<br>
								Apellido:
								<input type="text" name="apellido" id="apellido" placeholder="Digite su apellido" class="form-control">
								<br>
								NIT:
								<input type="text" name="nit" id="NIT" placeholder="Digite su nit" class="form-control" onblur="validar_nit();">
								<br>
								Telefono:
								<input type="text" name="telefono" id="telefono" placeholder="Digite su telefono" class="form-control">
								<br>
								Correo:
								<input type="text" name="correo" id="correo" placeholder="Digite su correo" class="form-control">
								<br>
								Direccion:
								<input type="text" name="direccion" id="direccion" placeholder="Digite su direccion" class="form-control">
								<br>
								<center>
									<button  type="button" class="btn btn-primary" onclick="agregar()">Guardar</button>
									<center>
									</form>
								</div>
							</div>

						</div>

						<script type="text/javascript">
							$(document).ready(function (){
								$("#disease,#vendedor,#MetodoPago").select2({
									allowClear:true,
									placeholder: 'Elija una option'
								});
								$('#NIT').mask('0000-000000-000-0');
								$('#telefono').mask('0000-0000');
						//datatable
						$('#example').DataTable();
					});

							function agregar(){
								var url='<?php echo base_url(); ?>clientesCtr/ingresar';
								$.ajax({
									url:url,
									type:'post',
									data: $('#form_cliente').serialize(),
									success: function(data){
										if(data ==1){
											Swal.fire({
												title: "Datos Ingresados Correctamente!",
												type: "success",
												showConfirmButton: false,
												timer: 1500

											});
											$('#nombre').val('');
											$('#apellido').val('');
											$('#nit').val('');
											$('#telefono').val('');
											$('#correo').val('');
											$('#direccion').val('');
											$('#exampleModal').modal('hide');


										}else{
											Swal.fire({
												title: "Error al ingresar!",
												type: "error",
												showConfirmButton: false,
												timer: 1500

											});

										}
									}
								});
							} 
							function guardar_factura(){
								Swal.fire({
									title: 'Confirmar Venta?',
									text: "Los datos guardados no se podran modificar!",
									type: 'warning',
									showCancelButton: true,
									confirmButtonColor: '#3085d6',
									cancelButtonColor: '#d33',
									confirmButtonText: 'Aceptar!'
								}).then((result) => {
									if (result.value) {
										var url='<?php echo base_url(); ?>ExisProducCtr/ingresar';
										$.ajax({
											url:url,
											type:'post',
											data: $('#registo').serialize(),
											success: function(data){
												if (data = "exito") {
													  window.location = "<?php echo base_url();?>start/home";
												}else{

												}
											}
										});
									}
								})
							}
							function validar_nit(){
								var telefono ='<?php echo base_url();?>clientesCtr/validar_nit';

								$.ajax(
								{
									type : 'post',
									url : telefono,
									data : 'NIT='+$('#NIT').val(),
									success:function(data){
										if (data==1) {
											swal.fire({
												type: 'error',
												title: 'Opps!',
												text: 'EL NIT YA EXISTE!'
											},
											function(isConfirm) {
												if (isConfirm) {
													$('#NIT').val('');
												}else{

												}
												$('#NIT').val('');
											});
										}else{
											
										}
									}
								});
							}
						</script>