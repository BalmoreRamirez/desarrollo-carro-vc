	<section class="full-width pageContent">
		<section class="full-width header-well">
			<div class="full-width header-well-icon">
				<i class="zmdi zmdi-card"></i>
			</div>
			<div class="full-width header-well-text">
				<p class="text-condensedLight">
					INGRESA EL CODIGO DEL AUTO, A VENDER PARA INICIAR EL PROCESO DE VENTA O VERIFICAR SUS DETALLES Y SU EXISTENCIA.
				</p>
			</div>
		</section>
		<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
			<div class="mdl-tabs__tab-bar">
				<a href="#tabNewPayment" class="mdl-tabs__tab is-active">NUEVA VENTA</a>

			</div>
			<div class="mdl-tabs__panel is-active" id="tabNewPayment">
				<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--8-col-desktop mdl-cell--2-offset-desktop">
						<div class="full-width panel mdl-shadow--2dp">
							<div class="full-width panel-tittle bg-primary text-center tittles">
								INGRESAR CODIGO DEL AUTO.
							</div>
							<div class="full-width panel-content">

								<h5 class="text-condensedLight">CODIGO DEL AUTO</h5>
								<form method="get" action="<?= base_url();?>ExisProducCtr/resultado">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input" type="text" id="resultado" name="resultado" required onblur="codigo()">
										<label class="mdl-textfield__label" for="NamePayment">CODIGO DEL AUTO</label>				
									</div>
									<input type="submit" value="Buscar" class="mdl-button md mdl-button--colored bg-primary ">
								</form>
								<!--boton que direccion a la vista-->								

								<h3> <?php if($this->session->flashdata('error')){?></h3>
								<div class="alert alert-danger"><?= $this->session->flashdata('error')?></div>
							<?php }?>
							<!---->

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>		
</section>
<script type="text/javascript">
	function codigo(){
		var val_codigo = '<?php echo base_url();?>start/val_cod';
		var resp = $('#resultado').serialize();
		$.ajax({
			type : 'post',
			url : val_codigo,
			data : $('#resultado').serialize(),
			success:function(data){
				if (data == 0){
					
					swal.fire({
						type: 'error',
						title: 'EL vehiculo no existe!',
						text: 'EL vehiculo no existe!',
						html:'<a href="<?php echo base_url();?>ExisProducCtr/encontrar?'+resp
						+'">Encontrar en mas sucursales</a>'
					},
					function(isConfirm) {
						if (isConfirm) {
							$('#resultado').val('');
						}else{
							
						}
						$('#resultado').val('');
					});
				}else{

				}
			}
		});
	}
</script>