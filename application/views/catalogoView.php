<style type="text/css">
#ads {
    margin: 30px 0 30px 0;
   
}

#ads .card-notify-badge {
        position: absolute;
        left: -10px;
        top: -20px;
        background: #f2d900;
        text-align: center;
        border-radius: 30px 30px 30px 30px; 
        color: #000;
        padding: 5px 10px;
        font-size: 14px;

    }

#ads .card-notify-year {
        position: absolute;
        right: -10px;
        top: -20px;
        background: #ff4444;
        border-radius: 50%;
        text-align: center;
        color: #fff;      
        font-size: 12px;      
        width: 50px;
        height: 50px;    
        padding: 15px 0 0 0;
}
#ads .card-detail-badge {      
        background: #f2d900;
        text-align: center;
        border-radius: 30px 30px 30px 30px;
        color: #000;
        padding: 5px 10px;
        font-size: 14px;        
    }
#ads .card:hover {
            background: #fff;
            box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
            border-radius: 4px;
            transition: all 0.3s ease;
        }

#ads .card-image-overlay {
        font-size: 20px;     
    }
#ads .card-image-overlay span {
            display: inline-block;              
        }


#ads .ad-btn {
        text-transform: uppercase;
        width: 150px;
        height: 40px;
        border-radius: 80px;
        font-size: 12px;
        line-height: 35px;
        text-align: center;
        border: 3px solid #e6de08;
        display: block;
        text-decoration: none;
        margin: 20px auto 1px auto;
        color: #000;
        overflow: hidden;        
        position: relative;
        background-color: #e6de08;
    }

#ads .ad-btn:hover {
            background-color: #e6de08;
            color: #1e1717;
            border: 2px solid #e6de08;
            background: transparent;
            transition: all 0.3s ease;
            box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
        }

#ads .ad-title h5 {
        text-transform: uppercase;
        font-size: 12px;
	}
	.card{
		height:350px;

	}

</style>

<section class="full-width pageContent">
	<section class="full-width header-well">
			<div class="full-width header-well-icon">
				<i class="zmdi zmdi-card"></i>
			</div>
			<div class="full-width header-well-text">
				<p class="text-condensedLight">
					Catalogo de autos disponibles en la sucursal
				</p>
			</div>
		</section>
		<hr>
<div class="container">
	<div class="row" id="ads">
		<?php foreach($carros as $key): $img = $key['uno'];?>
		<div class="col-md-3">
		<div class="card rounded my-4">
		<div class="card-image">
		<span class="card-notify-badge"><?php echo $key['Modelo'];?></span>
		<span class="card-notify-year"><?php echo $key['año'];?></span>
		<img class="img-fluid" src="http://localhost/desarrollo-carro/<?=$img?>" alt="Alternate Text" />
		</div>
		<div class="card-image-overlay m-auto">
		<span class="card-detail-badge"><?php echo $key['Monto'];?></span>
		<span class="card-detail-badge my-1">Cantidad de carros</span>
		<span class="card-detail-badge"><?php echo $key['unidades'];?></span>
		</div>
		<div class="card-body text-center">
		<div class="ad-title m-auto">
		<h5><?php echo "Marca: ". $key['marca'];?></h5>
		</div>
		<a  href="<?php echo base_url();?>ExisProducCtr/resultado?resultado=<?php echo $key['id_auto'];?>"  class="ad-btn">Vender</a>
		</div>
		</div>
		</div>
		<?php endforeach;?>
	</div>
</div>
</section>