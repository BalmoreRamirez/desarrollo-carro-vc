<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ExisProducCtr extends CI_Controller {
//cargamos el modelo, para extraer informacion de la base de datos
	public function __construct()
	{
		parent:: __construct();
		$this->load->model("servidor");
		$this->load->model("validar_login");
	}

	public function resultado()	{


		if ($this->session->userdata('correo') !=NULL|| $this->session->userdata('correo')!='') {
			if ($this->session->userdata('rol') == '1') {

				$user = $this->session->userdata('correo');
				$id = $this->validar_login->user($user);
				$var = $_GET['resultado'];
				$id_sucursal = $id->id_sucursal;
				$data['result'] = $this->servidor->consultas($var,$id_sucursal);

				if ($data['result'] == '0') {
					$this->session->set_flashdata('error', 'presione el boton de buscar antes');
					redirect('start/home');
				}else{
					$user = $this->session->userdata('correo');
					$data['user'] = $this->validar_login->user($user);
					$data['result'] = $this->servidor->consultas($var,$id_sucursal);
					$data['auto'] = $this->servidor->detalles_auto($var);
					$data['color'] = $this->servidor->color($var,$id_sucursal);
					$this->load->view('layout/header');
					$this->load->view('layout/menu',$data);
					$this->load->view('ExisProduc',$data);
					$this->load->view('layout/footer');

				}
			}else{
				$this->session->set_flashdata('error', 'usted no tiene acceso');
				redirect('start/index');
			}
			
		}else{
			redirect('start/index');
		}	
		
		
	}
	public function encontrar(){
		if ($this->session->userdata('correo') !=NULL|| $this->session->userdata('correo')!='') {
			if ($this->session->userdata('rol') == '1') {
				$var = $_GET['resultado'];
				$data['result'] = $this->servidor->consulta($var);


				if ($data['result'] == '0') {
					$this->session->set_flashdata('error', 'el veiculo no existe en otras sucursales');
					redirect('start/home');
				}else{
					$user = $this->session->userdata('correo');

					$data['user'] = $this->validar_login->user($user);
					$this->load->view('layout/header');
					$this->load->view('layout/menu',$data);
					$this->load->view('user/autos_usc',$data);
					$this->load->view('layout/footer');

				}

			}else{
				$this->session->set_flashdata('error', 'usted no tiene acceso');
				redirect('start/index');
			}
			
		}else{
			$this->load->view('layout/header');
			$this->load->view('login');
			$this->load->view('layout/footer');
		}	
		
	}
	public function factura(){
		if ($this->session->userdata('correo') !=NULL|| $this->session->userdata('correo')!='') {
			if ($this->session->userdata('rol') == '1') {
				$user = $this->session->userdata('correo');
				$id_suc = $this->validar_login->user($user);
				$id_sucursal = $id_suc->id_sucursal;
				$data['auto'] = $this->servidor->detalles_autos($id_sucursal);
				$data['user'] = $this->validar_login->user($user);
				$data['id_fac'] = $this->servidor->factura();
				$data['Cliente'] = $this->servidor->cliente();
				$data['MetodoPago'] = $this->servidor->metodo_pago();
				$data['ven'] = $this->servidor->vendedor();
				$data['autos'] = $this->servidor->produ($id_sucursal);
				$data['suma_total']= $this->servidor->total($id_sucursal);
				$this->load->view('layout/header');
				$this->load->view('layout/menu',$data);
				$this->load->view('user/factura',$data);
				$this->load->view('layout/footer');
			}else{
				$this->session->set_flashdata('error', 'usted no tiene acceso');
				redirect('start/index');
			}
			
		}else{
			$this->load->view('layout/header');
			$this->load->view('login');
			$this->load->view('layout/footer');		
		}
	}
	public function autos_mas_fac(){
		$var =  $this->uri->segment(3);
		$auto = $this->servidor->detalles_auto($var);
	}
	public function Agregar_producto(){

		$user = $this->session->userdata('correo');
		$id_suc = $this->validar_login->user($user);
		$id_sucursal = $id_suc->id_sucursal;
		$data['autos'] = $this->servidor->produ($id_sucursal);
		$this->load->view('user/autos',$data);
	}
	public function total(){
		$user = $this->session->userdata('correo');
		$id_suc = $this->validar_login->user($user);
		$id_sucursal = $id_suc->id_sucursal;
		$total= $this->servidor->total($id_sucursal);
		echo '<h6>'.$total->suma_total.'</h6>';
	}
	public function ver_aut(){
		$id_carro =  $this->uri->segment(3);
		$user = $this->session->userdata('correo');
		$id_suc = $this->validar_login->user($user);

		$stock = $this->servidor->stock($id_carro);

		if ($stock->unidades > 1) {
			$data['id_usuario'] = $id_suc->id_sucursal;
			$data['id_carro'] = $id_carro;
			$this->servidor->agregar_al_carrito($data);
			redirect('ExisProducCtr/factura');
		}else{
				$this->session->set_flashdata('error', 'No Hay Unidades Suficiente Para Poder Efectuar la Venta');
				redirect('ExisProducCtr/factura');
		}

		
	}
	public function delete(){
		$id_carrito = $this->uri->segment(3);
		$this->servidor->delete_carrito($id_carrito);
		redirect('ExisProducCtr/factura');
	}
	public function ingresar(){

		$user = $this->session->userdata('correo');
		$id_suc = $this->validar_login->user($user);
		
		$id_sucursal = $id_suc->id_sucursal;
		$suma_total= $this->servidor->total($id_sucursal);
		$numero_factura = $this->servidor->factura();
		$iva = $suma_total->suma_total * 0.013;
		
		$data['id_cliente'] = $_POST['cliente'];
		$data['id_vendedor'] = $_POST['vendedor'];
		$data['id_Metodopago'] = $_POST['pago'];
		$data['fecha'] =  date("Y-m-d");
		$data['num_factura'] = ($numero_factura->num_factura+1);
		$data['iva'] = $iva;
		$data['id_sucursal'] = $id_sucursal;
		$data['monto'] = $suma_total->suma_total+$iva;
		$carrito= $this->servidor->carrito($id_sucursal);
		$num = $this->servidor->facturaS($id_sucursal);
		$id_factura = $numero_factura->id_factura;
		$this->servidor->insertar_factura($data);

				
		foreach ($carrito as $key) {

			$dato['id_factura'] = $id_factura;
			$dato['id_sau'] = $key['id_carro'];
			$Unidades['unidades'] = $key['unidades'] -1;
			$id_sau = $key['id_carro'];
			echo $this->servidor->insert($dato);
			$this->servidor->update_unidad($Unidades,$id_sau);
		}
		redirect('ExisProducCtr/delete_carrito');
	}
	public function delete_carrito(){
		$user = $this->session->userdata('correo');
		$id_suc = $this->validar_login->user($user);
		$id_usuario = $id_suc->id_sucursal;
		$this->servidor->delet_carrito($id_usuario);
		redirect('start/home');
	}
}
/*usam 
usam2014
eels032 pas 77810*/