<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class clientesCtr extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('clientes_model');
	}

	public function index()
	{
		$this->load->view('user/factura');
	}

	public function ingresar()
	{
		$data['nombre'] = $_POST['nombre'];
		$data['apellido'] = $_POST['apellido'];
		$data['nit'] = $_POST['nit'];
		$data['telefono'] = $_POST['telefono'];
		$data['correo'] = $_POST['correo'];
		$data['direccion'] = $_POST['direccion'];
		echo $this->clientes_model->insertar($data);
		redirect('ExisProducCtr/factura');
	}
	public function validar_nit(){
		$NIT = trim($this->input->post('NIT'));
		echo $this->clientes_model->GET_NIT($NIT);
	}
}