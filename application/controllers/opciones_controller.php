<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class opciones_controller extends CI_Controller {
//cargamos el modelo, para extraer informacion de la base de datos
	public function __construct()
	{
		parent:: __construct();
		$this->load->model("servidor");
		$this->load->model("validar_login");
	}
	public function ver_opciones(){

		if ($this->session->userdata('correo') !=NULL|| $this->session->userdata('correo')!='') {
			if ($this->session->userdata('rol') == '1') {
				$var =  $this->uri->segment(3);
				$id_sucursal =  $this->uri->segment(4);
				$data['auto'] = $this->servidor->opcion_detalle($var,$id_sucursal);
				$user = $this->session->userdata('correo');
				$data['result'] = $this->servidor->consultas($var,$id_sucursal);
				$data['color'] = $this->servidor->color($var,$id_sucursal);
				$data['user'] = $this->validar_login->user($user);
				$this->load->view('layout/header');
				$this->load->view('layout/menu',$data);
				$this->load->view('ExisProduc',$data);
				$this->load->view('layout/footer');
			}else{
				$this->session->set_flashdata('error', 'usted no tiene acceso');
				redirect('start/index');
			}
			
		}else{
			redirect('start/index');
		}			
	}
}