<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CatalogoCrt extends CI_Controller {
//cargamos el modelo, para extraer informacion de la base de datos
	public function __construct()
	{
		parent:: __construct();
		$this->load->model("servidor");
		$this->load->model("validar_login");
	}


	public function cargarCatalogo()
	{	
		if ($this->session->userdata('correo') !=NULL|| $this->session->userdata('correo')!='') {
			if ($this->session->userdata('rol') == '1') {

				$user = $this->session->userdata('correo');
				$data['user'] = $this->validar_login->user($user);
				$id = $this->validar_login->user($user);
				$id_sucursal = $id->id_sucursal;

			//cargamos la informacion de carros
				$data['carros'] = $this->servidor->cargarCarros($id_sucursal);

				$this->load->view('layout/header');
				$this->load->view('layout/menu',$data);
				$this->load->view('catalogoView',$data);
				$this->load->view('layout/footer');

			}else{
				$this->session->set_flashdata('error', 'usted no tiene acceso');
				redirect('start/index');
			}

		}else{
			redirect('start/index');
		}	
		
	}
}
