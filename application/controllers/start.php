<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class start extends CI_Controller {
//==========================================================
	public function __construct(){
		parent:: __construct();
		$this->load->model('validar_login');
	}
//*==========================================================
	public function index()
	{
		if ($this->session->userdata('correo') !=NULL|| $this->session->userdata('correo')!='') {
			if ($this->session->userdata('rol') == '1') {

				redirect('start/home');

			}else{
				$this->session->set_flashdata('error', 'usted no tiene acceso');
				redirect('start/index');
			}
			
		}else{
			$this->load->view('layout/header');
			$this->load->view('login');
			$this->load->view('layout/footer');
		}	
	}
//==============================================================
	public function validar_session(){
		$user = $_POST['correo'];
		$pass = $_POST['password'];

		$valido = $this->validar_login->validar($user,$pass);
		if($valido != 'fail'){

			$this->session->set_userdata('correo',$user);
			$this->session->set_userdata('rol',$valido);

			if ($valido == '1') {
				redirect('start/home');
			}else{
				$this->session->set_flashdata('error', 'usted no tiene acceso');
				redirect('start/index');
			}
		}else{
			$this->session->set_flashdata('error', 'usuario o contraseña invalidos');
			redirect('start/index');
		}
	}
//==============================================================
	public function home()
	{
		if ($this->session->userdata('correo') !=NULL|| $this->session->userdata('correo')!='') {
			if ($this->session->userdata('rol') == '1') {

				$user = $this->session->userdata('correo');

				$data['user'] = $this->validar_login->user($user);

				$this->load->view('layout/header');
				$this->load->view('layout/menu',$data);
				$this->load->view('Home');
				$this->load->view('layout/footer');

			}else{
				$this->session->set_flashdata('error', 'usted no tiene acceso');
				redirect('start/index');
			}

		}else{
		redirect('start/index');
		}	
		
	}
	public function val_cod(){
		
		$user = $this->session->userdata('correo');
		$user = $this->validar_login->user($user);

		   $resultado = trim($this->input->post('resultado'));

		   $id_sucursal = $user->id_sucursal;
		
		 echo $this->validar_login->verificar_cod($resultado,$id_sucursal);
	}

	public function cerrar_sesion(){
		$this->session->sess_destroy();
		redirect('start/index');
	}
//==============================================================
}
