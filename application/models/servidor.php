<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class servidor extends CI_Model{


//hace una consulta para a veriguar la existencia de prpductos
	public function consulta($var){
		$result = $this->db->query("SELECT sau.*,aut.*,suc.*,marc.* FROM tab_sucursal_auto sau 
			INNER JOIN tab_auto aut ON sau.id_auto = aut.id_auto 
			INNER JOIN tab_sucursal suc ON sau.id_sucursal = suc.id_sucursal  
			INNER JOIN tab_marca marc ON aut.Marca = marc.id_marca 
			WHERE sau.id_auto ='".$var."'");

		if ($result->num_rows()>0) {
			return $result->result_array();
		}else{
			return 0;
		}
	}
	public function consultas($var,$id_sucursal)
	{
		$result = $this->db->query("SELECT sau.*,sau.id_sau as suct,aut.*,suc.*,marc.*,trap.*,tip.*,img.* FROM tab_sucursal_auto sau 
			INNER JOIN tab_auto aut ON sau.id_auto = aut.id_auto 
			INNER JOIN tab_sucursal suc ON sau.id_sucursal = suc.id_sucursal  
			INNER JOIN tab_tipoauto tip ON aut.tipo_cuerpo = tip.id_tipo 
			INNER JOIN tab_traccion trap ON aut.traccion = trap.id_traccion 
			INNER JOIN tab_marca marc ON aut.Marca = marc.id_marca 
			INNER JOIN tab_img_autos img ON aut.id_fotos = img.id_img
			WHERE sau.id_auto ='".$var."' AND sau.id_sucursal ='".$id_sucursal."'");

		if ($result->num_rows()>0) {
			return $result->row();
		}else{
			return 0;
		}
	}
	public function produ($id_sucursal){
		$result = $this->db->query("SELECT sau.*,col.*,aut.*,tip.*,marc.* FROM tab_sucursal_auto sau 
			INNER JOIN tab_colores col ON sau.id_color = col.id_colores
			INNER JOIN tab_auto aut ON sau.id_auto = aut.id_auto 
			INNER JOIN tab_tipoauto tip ON aut.tipo_cuerpo = tip.id_tipo 
			INNER JOIN tab_marca marc ON aut.Marca = marc.id_marca 
			WHERE sau.id_sucursal = '".$id_sucursal."' GROUP BY aut.Modelo,col.id_colores");
		return $result->result_array();
	}
	public function stock($id_sau){
		$result = $this->db->query("SELECT * FROM tab_sucursal_auto
			WHERE id_sau ='".$id_sau."'");
		return $result->row();
	}
	public function detalles_auto($var){
		$query = $this->db->query("SELECT sau.id_sau,sau.id_auto,suc.nombre,sau.unidades,marc.marca,aut.Modelo,aut.año,tip.tipo,aut.Monto,aut.tipo_motor,aut.cilindros,aut.transmision,trap.traccion,aut.combustible,aut.stock,img.* FROM tab_sucursal_auto sau 
			INNER JOIN tab_auto aut ON sau.id_auto = aut.id_auto 
			INNER JOIN tab_sucursal suc ON sau.id_sucursal = suc.id_sucursal  
			INNER JOIN tab_tipoauto tip ON aut.tipo_cuerpo = tip.id_tipo 
			INNER JOIN tab_traccion trap ON aut.traccion = trap.id_traccion 
			INNER JOIN tab_marca marc ON aut.Marca = marc.id_marca 
			INNER JOIN tab_img_autos img ON aut.id_fotos = img.id_img 
			WHERE sau.id_auto ='".$var."'");
		return $query->row();
	}
	public function opcion_detalle($var,$id_sucursal){
		$query = $this->db->query("SELECT sau.id_sau,sau.id_auto,suc.nombre,sau.unidades,marc.marca,aut.Modelo,aut.año,tip.tipo,aut.Monto,aut.tipo_motor,aut.cilindros,aut.transmision,trap.traccion,aut.combustible,aut.stock,img.* FROM tab_sucursal_auto sau 
			INNER JOIN tab_auto aut ON sau.id_auto = aut.id_auto 
			INNER JOIN tab_sucursal suc ON sau.id_sucursal = suc.id_sucursal  
			INNER JOIN tab_tipoauto tip ON aut.tipo_cuerpo = tip.id_tipo 
			INNER JOIN tab_traccion trap ON aut.traccion = trap.id_traccion 
			INNER JOIN tab_marca marc ON aut.Marca = marc.id_marca 
			INNER JOIN tab_img_autos img ON aut.id_fotos = img.id_img 
			WHERE sau.id_auto ='".$var."' AND sau.id_sucursal ='".$id_sucursal."'");
		return $query->row();
	}
	public function detalles_autos($id_sucursal){
		$query = $this->db->query("SELECT  COUNT(aut.id_auto)as suma,carri.*,sau.*,col.*,aut.*,mar.*,suc.*  FROM tab_carrito carri
			INNER JOIN tab_sucursal_auto sau ON carri.id_carro = sau.id_sau
			INNER JOIN tab_auto aut ON sau.id_auto = aut.id_auto 
			INNER JOIN tab_colores col ON sau.id_color = col.id_colores 
			INNER JOIN tab_marca mar ON aut.Marca = mar.id_marca
			INNER JOIN tab_sucursal suc ON sau.id_sucursal = suc.id_sucursal  
			WHERE  carri.id_usuario ='".$id_sucursal."'GROUP BY aut.Modelo,col.id_colores");
		return $query->result_array();
	}
	public function total($id_sucursal){
		$query = $this->db->query("SELECT SUM(aut.Monto) as suma_total FROM tab_carrito carri 
			INNER JOIN tab_sucursal_auto sau ON  carri.id_carro = sau.id_sau
			INNER JOIN tab_auto aut ON sau.id_auto = aut.id_auto 
			WHERE carri.id_usuario ='".$id_sucursal."'");
		return $query->row();
	}
	public function color($var,$id_sucursal){
		$query = $this->db->query("SELECT col.* FROM tab_sucursal_auto sau 
			INNER JOIN tab_colores col ON sau.id_color = col.id_colores 
			WHERE sau.id_auto ='".$var."'AND sau.id_sucursal ='".$id_sucursal."' GROUP BY col.id_colores");
		return $query->result_array();
	}
	public function carrito($id_sucursal){
		$query = $this->db->query("SELECT carri.*,suc.* FROM tab_carrito carri
			INNER JOIN tab_sucursal_auto suc ON carri.id_carro = suc.id_sau 
			WHERE carri.id_usuario ='".$id_sucursal."'");
		return $query->result_array();
	}
	public function factura(){
		$query = $this->db->query("SELECT * FROM `tab_factura` WHERE num_factura = (SELECT max(num_factura) FROM tab_factura)");
		return $query->row();
	}
	public function facturaS($id_sucursal){
		$query = $this->db->query("SELECT max(id_factura) FROM tab_factura WHERE id_sucursal ='".$id_sucursal."'");
		return $query->row();
	}
	public function cliente(){
		$query = $this->db->query("SELECT * FROM `tab_cliente` ");
		return $query->result_array();
	}

	public function agregar_al_carrito($data){
		$this->db->insert('tab_carrito',$data);
	}
	public function delete_carrito($id_carrito){
		$this->db->where('id_carrito =',$id_carrito);
		$this->db->delete('tab_carrito');
	}
	public function vendedor(){
		$query = $this->db->get('tab_vendedor');
		return $query->result_array();
	}
	public function metodo_pago(){
		$query = $this->db->get('tab_metodo_pagos');
		return $query->result_array();
	}
	public function insertar_factura($data){
		$this->db->insert('tab_factura',$data);
	}
	public function insert($dato){
		if($this->db->insert('tab_venta',$dato)){
			echo "exito";
		}else{
			echo "error";
		}
	}
	public function delet_carrito($id_usuario){
		$this->db->where('id_usuario =',$id_usuario);
		$this->db->delete('tab_carrito');
	}
	public function cargarCarros($id_sucursal)
	{
		$this->db->set('au.*, m.marca,img.uno');
		$this->db->where('sa.id_auto = au.id_auto AND au.Marca = m.id_marca AND au.id_fotos = img.id_img AND id_sucursal=',$id_sucursal);
		$data = $this->db->get('tab_sucursal_auto sa,tab_auto au,tab_marca m,tab_img_autos img');
		return $data->result_array();
	}
	public function update_unidad($Unidades,$id_sau){
		$this->db->where('id_sau',$id_sau);
		$this->db->update('tab_sucursal_auto',$Unidades);
	}
	
}